#!/bin/bash
DEF_REPS=20
AST_REPS=5
FILENAME="default"

function ncl {
	FNAME=$1
	FNAME=${FNAME%.nacl}
	eval $NACLPATH -c $1 > /dev/null
	eval $REACTORPATH "$FNAME.mltn"
	rm "$FNAME.mltn"
}

function csvgen {
	echo -e "\t" $METHOD: $*
	OUTNAME=$TESTNAME"_"$METHOD
	eval perf stat -v -r $REPS $* 2>&1> /dev/null | grep 'task-clock:u:' > temp
	rm -f ./out/$OUTNAME.out
	touch ./out/$OUTNAME.out
	while read r; do
		read -ra t <<< "$r"
		echo ${t[2]} >> ./out/$OUTNAME.out
	done < temp
	rm -f temp
}

# source settings
. ./settings.cfg
 
for dir in ./benchmarks/*
do
	REPS=$DEF_REPS
	. $dir/name.cfg
	dir=${dir%*/}
	echo "Executing benchmarks in:" $dir
	METHOD="c"
	FILENAME=$TESTNAME"_c" 
	csvgen $dir/$FILENAME

	METHOD="rust"
	FILENAME=$TESTNAME"_rs" 
	csvgen $dir/$FILENAME
	
	METHOD="java_jitless"
	FILENAME=$TESTNAME".java" 
	csvgen java -Xint $dir/$FILENAME

	METHOD="java_jit"
	FILENAME=$TESTNAME".java" 
	csvgen java $dir/$FILENAME

	METHOD="python"
	FILENAME=$TESTNAME".py" 
	csvgen python3 $dir/$FILENAME

	METHOD="node_jitless"
	FILENAME=$TESTNAME".js" 
	csvgen node --jitless $dir/$FILENAME

	METHOD="node_jit"
	FILENAME=$TESTNAME".js" 
	csvgen node $dir/$FILENAME

	METHOD="reactor"
	FILENAME=$TESTNAME".nacl" 
	csvgen ./util/salt.sh $dir/$FILENAME $NACLPATH $REACTORPATH

	REPS=$AST_REPS
	METHOD="nacl_ast"
	FILENAME=$TESTNAME".nacl" 
	csvgen $NACLPATH $dir/$FILENAME 
	echo ""
done
