# NaCl Benchmark

This repository contains a few benchmarks to analyze the performance of the 
NaCl interpreters ([NaCl AST Interpreter](https://gitlab.com/nacl-lang/nacl) 
as well as the [Reactor Virtual Machine](https://gitlab.com/nacl-lang/reactor)
) compared to other interpreted as well as compiled and JIT-compiled languages.

In the current version, the following languages are benchmarked:
- Java (`-Xint` as well as JIT-compiled)
- JavaScript (node, `--jitless` as well as JIT-compiled)
- C (gcc-default opt_level)
- Rust (rustc-default opt level)
- Python
- NaCl (Reactor VM and NaCl AST-Interpreter)

## Current Benchmarks

At the moment, the repository contains the following benchmarks:

| Name | Benchmark |
| ------  | ----------- |
| `fib`     | Calculate the 40th Fibonacci number recursively (lazy) |
| `pprime`  | Calculate the 200th prime number that is a palindrome in base 10 |
| `fac-it`  | Calculate 20! 1.000.000 times iteratively |
| `fac-rec` | Calculate 20! 1.000.000 times recursively |

## Running the benchmark

The main benchmark code is contained in [bench.sh](bench.sh), settings are 
contained in [settings.cfg](settings.cfg) where NaCl and Reactor paths
can be set. Obviously, all languages listed above have to be installed to run
the benchmarks. For node.js, at least version 12 is needed (for the `--jitless`
option). Furthermore, the benchmark uses `perf` to retrieve the time needed
in milliseconds.

bench.sh produces an output file (`TESTNAME_METHOD.out`) in the out folder for 
every test that is run. This file contains the running time in nanoseconds for 
every run of the benchmark and can be used to make statistics about the overall 
performance.
