fn fib(a:isize) -> isize {
    if a < 2 {
        return a;
    }
    return fib(a-1) + fib(a-2);
}

fn main() {
    println!("{}", fib(40));
}
