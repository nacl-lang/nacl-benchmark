public class sin {
	public static void main(String[] args) {
		double pi = 3.14159264359;
		int iterations = 1000000;
		for (int i=0; i<iterations; i++)
		{
			double x = pi * (i/iterations);
			sin(x);
		}
	}

	static double sin(double x){
		double dividend = x;
		double divisor = 1;
		double faculty = 1;
		double sign = 1;
		double result = x;
			
		int iterations = 15;
		int current_iteration = 0;
		while (current_iteration < iterations) {
			dividend = dividend * x * x;
			divisor = divisor * (faculty + 1.0) * (faculty + 2.0);
			faculty = faculty + 2.0;
			sign = sign * -1.0;

			result = result + (dividend / divisor * sign);
			current_iteration = current_iteration + 1;
		}
		return result;	
	}
}
