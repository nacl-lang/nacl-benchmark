#!/usr/bin/python3 -u

def sin(x):
    dividend = x
    divisor = 1.0
    faculty = 1.0
    sign = 1.0
    result = x

    iterations = 15
    current_iteration = 0
    while current_iteration < iterations: 
        dividend = dividend * x * x
        divisor = divisor * (faculty + 1.0) * (faculty + 2.0)
        faculty = faculty + 2.0
        sign = sign * -1.0

        result = result + (dividend / divisor * sign)
        current_iteration = current_iteration + 1

    return result;

if __name__ == "__main__":
	pi = 3.14159264359
	i = 0
	iterations = 1000000
	while (i < iterations):
		x = sin(pi * (i / iterations))
		i = i + 1
        
