fn sin(x: f64) -> f64 
{
    let mut dividend = x;
    let mut divisor = 1.0;
    let mut faculty = 1.0;
    let mut sign = 1.0;
    let mut result = x; /* First iteration with n = 0 */

    let iterations = 15;
    let mut current_iteration = 0;
    while current_iteration < iterations
    {
        dividend = dividend * x * x;
        divisor = divisor * (faculty + 1.0) * (faculty + 2.0);
        faculty = faculty + 2.0;
        sign = sign * -1.0;

        result = result + (dividend / divisor * sign);
        current_iteration = current_iteration + 1;
    }

    return result;
}

fn main() {
	let pi: f64 = 3.14159264359;
	let mut i: i32 = 0;
	let iterations: i32 = 1000000;
	while i < iterations {
		sin(pi * (i as f64 / iterations as f64));
		i = i + 1;
	}
}
        
