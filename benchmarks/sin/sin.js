function sin(x){
	var dividend = x;
	var divisor = 1;
	var faculty = 1;
	var sign = 1;
	var result = x;
		
	var iterations = 15;
	var current_iteration = 0;
	while (current_iteration < iterations) {
		dividend = dividend * x * x;
		divisor = divisor * (faculty + 1.0) * (faculty + 2.0);
		faculty = faculty + 2.0;
		sign = sign * -1.0;

		result = result + (dividend / divisor * sign);
		current_iteration = current_iteration + 1;
	}
	return result;	
}

var pi = 3.14159264359;
var iterations = 1000000;
for (var i=0; i<iterations; i++)
{
	var x = pi * (i/iterations);
	sin(x);
}

