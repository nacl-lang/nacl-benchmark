double sin(double x) 
{
	double dividend = x;
    double divisor = 1.0;
    double faculty = 1.0;
    double sign = 1.0;
    double result = x; /* First iteration with n = 0 */

    int iterations = 15;
    int current_iteration = 0;
    while (current_iteration < iterations) 
    {
        dividend = dividend * x * x;
        divisor = divisor * (faculty + 1.0) * (faculty + 2.0);
        faculty = faculty + 2.0;
        sign = sign * -1.0;

        result = result + (dividend / divisor * sign);
        current_iteration = current_iteration + 1;
    }

    return result;
}

int main(int argc, char** argv) {
	double pi = 3.14159264359;
	int i = 0;
	int iterations = 1000000;
	while (i < iterations) {
		double x = sin(pi * (i / iterations));
		i = i + 1;
	}
	
	return 0;
}
