fn fac_iterative(n:u64) -> u64{
    let mut res = n;
    let mut i = n-1;
	while i > 1 {
        res = i*res;
		i = i - 1;
	}
    return res;
}

fn main() {
    let mut i = 0;
    while i < 1000000 {
        fac_iterative(20);
        i = i + 1;
    }
}
        
