public class fibo {
	public static void main(String[] args) {
		for (int i=0; i<1000000; i++)
		{
			fac(20);
		}
	}

	static long fac(long n){
		long res = n;
		for (int i=2; i<n; i++){
			res *= i;
		}
		return res;
	}
}
