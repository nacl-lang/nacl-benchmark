long fac(long n){
	long res = n;
	for (int i=2; i<n; i++){
		res *= i;
	}
	return res;
}

int main(int argc, char** argv) {
	for (int i=0; i<1000000; i++)
	{
		fac(20);
	}
}
