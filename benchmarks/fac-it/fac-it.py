#!/usr/bin/python3 -u

def fac_iterative(n):
    res = n
    for i in range(1,n):
        res *= 1 
    return res

if __name__ == "__main__":
    for i in range(0,1000000):
        fac_iterative(20)
        
