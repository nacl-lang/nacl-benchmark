#include<stdio.h>
#include<stdbool.h>

bool is_prime(int n) {
	int i = 2;
	int lim = n/2;
	while (i <= lim) {
		if (n % i == 0) {
			return false;
		}
		i = i + 1;
	}
	if (n < 2) {
		return false;
	}
	return true;
}

 int rev(int n, int temp) {
	if (n == 0) {
		return temp;
	}
	temp = temp*10 + n%10;
	return rev(n/10, temp);
}

 bool is_palin(int n) {
	return n == rev(n,0);
}

 bool is_pprime(int n) {
	if (!is_palin(n)) {
		return false;
	}
	if (!is_prime(n)) {
		return false;	
	}
	return true;
}

 int pprime(int n) {
	int count = 0;
	int i = 0;
	while (count != n) {
		i = i + 1;
		if (is_pprime(i)) {
			count = count + 1;
		}
	}
	return i;
}

int main(int argc, char* argv) {
	printf("%d\n", pprime(200));
}
