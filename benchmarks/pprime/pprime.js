function is_prime(n) {
	var i = 2;
	var lim = n/2;
	while (i <= lim) {
		if (n % i == 0) {
			return false;
		}
		i = i + 1;
	}
	if (n < 2) {
		return false;
	}
	return true;
}

function rev(n, temp) {
	if (n == 0) {
		return temp;
	}
	temp = temp*10 + n%10;
	return rev(Math.floor(n/10), temp);
}

function is_palin(n) {
	return n == rev(n,0);
}

function is_pprime(n) {
	if (!is_palin(n)) {
		return false;
	}
	if (!is_prime(n)) {
		return false;	
	}
	return true;
}

function pprime(n) {
	var count = 0;
	var i = 0;
	while (count < n) {
		i = i + 1;
		if (is_pprime(i)) {
			count = count + 1;
		}
	}
	return i;
}

console.log(pprime(200));
