fn is_prime(n: isize) -> bool {
	let mut i = 2;
	let lim = n/2;
	while i <= lim {
		if n % i == 0 {
			return false;
		}
		i = i + 1;
	}
	if n < 2 {
		return false;
	}
	return true;
}

fn rev(n:isize, temp:isize) -> isize {
	if n == 0 {
		return temp;
	}
	let temp = temp*10 + n%10;
	return rev(n/10, temp);
}

fn is_palin(n:isize) -> bool {
	return n == rev(n,0);
}

fn is_pprime(n: isize) -> bool {
	if !is_palin(n) {
		return false;
	}
	if !is_prime(n) {
		return false;	
	}
	return true;
}

fn pprime(n:isize) -> isize{
	let mut count = 0;
	let mut i = 0;
	while count != n {
		i = i + 1;
		if is_pprime(i) {
			count = count + 1;
		}
	}
	return i;
}

fn main() {
    println!("{}",pprime(200));
}
