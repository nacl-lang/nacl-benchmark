def is_pprime(n):
    if not is_palin(n):
        return False
    if not is_prime(n):
        return False
    return True

def is_prime(n):
    for i in range(2,n//2+1):
        if n % i == 0:
            return False
    if n < 2:
        return False
    return True

def is_palin(n):
    return n == rev(n,0)

def rev(n, temp):
    if (n == 0):
        return temp;
    temp = (temp * 10) + (n % 10);
    return rev(n // 10, temp);

def pprime(n):
    count = 0
    i = 0
    while count != n:
        i += 1
        if is_pprime(i):
            count += 1
    return i

if __name__ == "__main__":
    print(pprime(200))
