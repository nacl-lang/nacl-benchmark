fn fac(n:isize) -> isize{
    if n < 2 {
        return n;
    }
    return fac(n-1)*n;
}

fn main() {
    let mut i = 0;
    while i < 1000000 {
        fac(20);
        i = i + 1;
    }
}
        
