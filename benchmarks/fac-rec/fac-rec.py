#!/usr/bin/python3 -u

def fac(n):
    if n <= 2:
        return n;
    return fac(n-1)*n

if __name__ == "__main__":
    for i in range(0,1000000):
        fac(20)
