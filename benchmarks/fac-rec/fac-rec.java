public class fibo {
	public static void main(String[] args) {
		for (int i=0; i<1000000; i++)
		{
			fac(20);
		}
	}

	static long fac(long n){
		if (n <= 2) {
			return n;
		}
		return fac(n-1)*n;
	}
}
